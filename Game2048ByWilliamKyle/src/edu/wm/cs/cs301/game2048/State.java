package edu.wm.cs.cs301.game2048;
import java.util.*; 
import java.util.function.Supplier;
import java.util.Random;
import java.util.Arrays;  

public class State implements GameState {
	int[] tiles = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	public State(State currentState) {
		//Empty because I figured out a way to do it without it
		
	}

	public State() {
		//^^
	}

	@Override
	public int getValue(int x, int y) {
		return tiles[x + y*4];
	}

	@Override
	public void setValue(int x, int y, int value) {
		tiles[x+y*4] = value;
		
		// This method sets the inputed value at the given index
	}

	@Override
	public void setEmptyBoard() {
		for (int i = 0; i < 16; i++) {
			tiles[i] = 0;
		}
		// Empties the board
	}

	@Override
	public boolean addTile() {
		// adds either a 2 or a 4 randomly
		if (!isFull()) {
			Random rand = new Random();
			int randomlocation = rand.nextInt(16);
			while (tiles[randomlocation]!= 0) {
				randomlocation = rand.nextInt(16);
			}
			int twoorfour = rand.nextInt(2);
			// this selects a random integer (0,1) and converts it to
			//a 2 or 4
			tiles[randomlocation] = (twoorfour+1)*2;
			return true;
		}
		else {
		return false;
	}
	}

	@Override
	public boolean isFull() {
		//checks to see if there is a single 0 on the board
		for (int i = 0; i < 16; i++) {
			if (tiles[i] == 0) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean canMerge() {

		//For up and down movements
		//Fixed release0.4 version
		
		for (int x = 0; x<4; x=x+1) {
			int counter = 0;
			for (int y = 0; y<12; y=y+4) {
				if (tiles[y+x]==tiles[y+x+4]) {
					counter +=1;
					if (counter>0) {
						return true;
					}
				}
			}
		}

		//For left and right movements
		for (int b = 0;b<4;b=b+1) {
			int counter = 0;
			for (int c = 0; c<3; c = c+1) {
				if (tiles[c+b*4] == tiles[c+b*4+1]) {
					counter +=1;
					if (counter > 0) {
					return true;
				}}
		}}
		return false;
	}

	@Override
	public boolean reachedThreshold() {
		// Checks to see if a single 2048 or higher is on the board
        for (int i = 0; i < 16; i++) {
			if (tiles[i] >= 2048) {
			    return true;
			}
		}
        return false;
	}

	@Override
	public int left() {
		////going left
		// A placeholder tile is used to put values in the correct spots,
        // afterwards, they are placed into the tile spots one by one.
		int score = 0;
        int[] savetiles = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        for (int x=0; x<16; x=x+4){
            int j=x;
            int check =-1;
            for (int i=x; i<x+4;i++){
                if (tiles[i] !=0){
                    if (check == -1){
                        check = tiles[i];
                    }
                    else{
                            if (check == tiles[i]){
                                savetiles[j] = 2*tiles[i];
                                score += savetiles[j];
                                j+=1;
                                check = -1;
                            }
                            else{
                                savetiles[j]=check;
                                j+=1;
                                check = tiles[i];
                            }
                        }
                    }
                }
            if (check != -1){
                savetiles[j] = check;
            }
}
        for (int x = 0;x<4; x++) {
        	for (int y = 0; y< 4; y++) {
        		setValue(x,y,savetiles[x+y*4]);}}
		return score;
    }
//////////////////////

	@Override
	public int right() {
		//right movement
		int score = 0;
		int[] savetiles = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		for (int x=15; x>-1; x=x-4){
            int j=x;
            int check =-1;
            for (int i=x; i>x-4;i=i-1){
                if (tiles[i] !=0){
                    if (check == -1){
                        check = tiles[i];
                    }
                    else{
                            if (check == tiles[i]){
                                savetiles[j] = 2*tiles[i];
                                score += savetiles[j];
                                j-=1;
                                check = -1;
                            }
                            else{
                                savetiles[j]=check;
                                j-=1;
                                check = tiles[i];
                            }
                        }
                    }
                }
            if (check != -1){
                savetiles[j] = check;
            }
}
        for (int x = 0;x<4; x++) {
        	for (int y = 0; y< 4; y++) {
        		setValue(x,y,savetiles[x+y*4]);}}
		return score;
	}

	@Override
	public int down() {
		//Down movement. 
		int score = 0;
        int[] savetiles = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        for (int x=12; x<16; x=x+1){
            int j=x;
            int check =-1;
            for (int i=x; i>-1; i=i-4){
                if (tiles[i] !=0){
                    if (check == -1){
                        check = tiles[i];
                    }
                    else{
                            if (check == tiles[i]){
                                savetiles[j] = 2*tiles[i];
                                score += savetiles[j];
                                j-=4;
                                check = -1;
                            }
                            else{
                                savetiles[j]=check;
                                j-=4;
                                check = tiles[i];
                            }
                        }
                    }
                }
            if (check != -1){
                savetiles[j] = check;
            }
}
        for (int x = 0;x<4; x++) {
        	for (int y = 0; y< 4; y++) {
        		setValue(x,y,savetiles[x+y*4]);}}
        return score;
	}

	@Override
	public int up() {
		//movement down
		int score = 0;
        int[] savetiles = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        for (int x=0; x<4; x=x+1){
            int j=x;
            int check =-1;
            for (int i=x; i<16;i=i+4){
                if (tiles[i] !=0){
                    if (check == -1){
                        check = tiles[i];
                    }
                    else{
                            if (check == tiles[i]){
                                savetiles[j] = 2*tiles[i];
                                score += savetiles[j];
                                j+=4;
                                check = -1;
                            }
                            else{
                                savetiles[j]=check;
                                j+=4;
                                check = tiles[i];
                            }
                        }
                    }
                }
            if (check != -1){
                savetiles[j] = check;
            }
}
        for (int x = 0;x<4; x++) {
        	for (int y = 0; y< 4; y++) {
        		setValue(x,y,savetiles[x+y*4]);}}

		return score;
	}
	//creating custom hashcode for the array
	public int hashCode() {
		final int prime = 23;
		int result = 1;
		result = prime*result + Arrays.hashCode(tiles);
		return result;
	}
	//Overriding the equals method so it works with this project
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if(obj == null || obj.getClass()!= this.getClass()) {
            return false; 
            
	}
		State compare = (State) obj;
		int count = 0;
		
		for (int i=0;i<16;i++) {
		if (this.tiles[i] == compare.tiles[i]) {
			count+=1;}
		}
		if (count == 16) {
			return true;
		}
		return false;
	}


}
